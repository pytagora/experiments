// ConsoleSmartPtr.cpp : Defines the entry point for the console application.
//

#include <iostream>
#include "stdafx.h"
#include "SmartPtr.h"

int main()
{
	SmartPtr ptr(new int());
	*ptr = 20;
	std::cout << *ptr;
	system("pause");
    return 0;
}