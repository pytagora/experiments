#pragma once
using namespace std;


class SmartPtr
{
	int *ptr;

public:
	explicit SmartPtr(int *p = nullptr) { ptr = p; }

	~SmartPtr() { delete(ptr); }

	int &operator *() { return *ptr; }
};